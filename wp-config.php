<?php
/**
 * In dieser Datei werden die Grundeinstellungen f�r WordPress vorgenommen.
 *
 * Zu diesen Einstellungen geh�ren: MySQL-Zugangsdaten, Tabellenpr�fix,
 * Secret-Keys, Sprache und ABSPATH. Mehr Informationen zur wp-config.php gibt es auf der {@link http://codex.wordpress.org/Editing_wp-config.php
 * wp-config.php editieren} Seite im Codex. Die Informationen f�r die MySQL-Datenbank bekommst du von deinem Webhoster.
 *
 * Diese Datei wird von der wp-config.php-Erzeugungsroutine verwendet. Sie wird ausgef�hrt, wenn noch keine wp-config.php (aber eine wp-config-sample.php) vorhanden ist,
 * und die Installationsroutine (/wp-admin/install.php) aufgerufen wird.
 * Man kann aber auch direkt in dieser Datei alle Eingaben vornehmen und sie von wp-config-sample.php in wp-config.php umbenennen und die Installation starten.
 *
 * @package WordPress
 */

// Force SSL
define('FORCE_SSL_ADMIN', true);


/**  MySQL Einstellungen - diese Angaben bekommst du von deinem Webhoster. */
/**  Ersetze database_name_here mit dem Namen der Datenbank, die du verwenden m�chtest. */
define('DB_NAME', 'HTO01FLQZNUW_2');

/** Ersetze username_here mit deinem MySQL-Datenbank-Benutzernamen */
define('DB_USER', 'root');

/** Ersetze password_here mit deinem MySQL-Passwort */
define('DB_PASSWORD', 'mariadb');

/** Ersetze localhost mit der MySQL-Serveradresse */
define('DB_HOST', 'localhost:3306');

/** Der Datenbankzeichensatz der beim Erstellen der Datenbanktabellen verwendet werden soll */
define('DB_CHARSET', 'utf8');

/** Der collate type sollte nicht ge�ndert werden */
define('DB_COLLATE', '');


/**#@+
 * Sicherheitsschl�ssel
 *
 * �ndere jeden KEY in eine beliebige, m�glichst einzigartige Phrase. 
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service} kannst du dir alle KEYS generieren lassen.
 * Bitte trage f�r jeden KEY eine eigene Phrase ein. Du kannst die Schl�ssel jederzeit wieder �ndern, alle angemeldeten Benutzer m�ssen sich danach erneut anmelden.
 *
 * @seit 2.6.0
 */
define('AUTH_KEY',         ' Pk4$Se6RX.2(&NMNEdL>MWqEVwlFmli+>l!4.rNt.Bz^o:#Z*$J?0P*8p:M-6+$');
define('SECURE_AUTH_KEY',  'Dw&O91w~r1ouf:hf,N.oVHZP4|&Y@?8(Cf9Q0h#n]vkd-xnk1em Dgb;S5|cgD4}');
define('LOGGED_IN_KEY',    's;#%Dd4%e.yD;g_.rDp|Xtn qUHip)PNr^&o_>}Oce~M6q#wj2G=PDH{im}QJ0 |');
define('NONCE_KEY',        'D!LwD-j-/8I3>T S.BdI<JF]B3gO2-#-}r/!9lNa*wL`B8I`8|8_uN#!Wu@<3*:F');
define('AUTH_SALT',        '/ev&G4c{xbg*juUsG&s}7pL^|J)d&UEp|`/it)XwKVKOnrI+x[*+5BJRJpFs{K7R');
define('SECURE_AUTH_SALT', '^>+[L$$87/Tl}F;xGALurb^Cz5V6Cd)_4o+D;sWT98 L/q%KQLa7f|z`aUV<ogEJ');
define('LOGGED_IN_SALT',   '* Q]A[+<aStL63bFpTNP2V[r)dv!C<|-8#B:nD~_9D3mk}wDU,gB0L8q;:imU&qR');
define('NONCE_SALT',       'n@{+7<d%j=>/8m%BphaB#V`.Rk:y>Lp#{Q(#X=6e/a=(PMJ.}l/KqFyP9IxO{0=I');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Pr�fix
 *
 *  Wenn du verschiedene Pr�fixe benutzt, kannst du innerhalb einer Datenbank
 *  verschiedene WordPress-Installationen betreiben. Nur Zahlen, Buchstaben und Unterstriche bitte!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Sprachdatei
 *
 * Hier kannst du einstellen, welche Sprachdatei benutzt werden soll. Die entsprechende
 * Sprachdatei muss im Ordner wp-content/languages vorhanden sein, beispielsweise de_DE.mo
 * Wenn du nichts eintr�gst, wird Englisch genommen.
 */
define('WPLANG', 'de_DE');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
