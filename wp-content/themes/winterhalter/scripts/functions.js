	$( document ).ready(function( $ ) {
		
		
		// Overlays
		$('.openoverlay-impressum').click(function(e){
			$( 'body' ).css( 'overflow', 'hidden' );
			$('#overlay-impressum').show();
			e.preventDefault();
		});

		$('.openoverlay-datenschutz').click(function(e){
			$( 'body' ).css( 'overflow', 'hidden' );
			$('#overlay-datenschutz').show();
			e.preventDefault();
		});

		$('.openoverlay-haftung').click(function(e){
			$( 'body' ).css( 'overflow', 'hidden' );
			$('#overlay-haftung').show();
			e.preventDefault();
		});
		
		$('.openoverlay-agb').click(function(e){
			$( 'body' ).css( 'overflow', 'hidden' );
			$('#overlay-agb').show();
			e.preventDefault();
		});
		
		$('.openoverlay-karriere').click(function(e){
			$( 'body' ).css( 'overflow', 'hidden' );
			$('#overlay-karriere').show();
			e.preventDefault();
		});

		$('.openoverlay-video').click(function(e){
			$( 'body' ).css( 'overflow', 'hidden' );
			$('#overlay-video').show();
			e.preventDefault();
		});


		
		$('.overlayclose').click(function(){

			$( 'body' ).css( 'overflow', 'auto' );
			$('.overlay').hide();	

		});
		
		




		// Karriere
		$('.karTit').click(function(){
		
			var openKar = $(this).attr('data-open');
			
			if($('#karCont-'+openKar).is(':visible')) {
				$('.karCont').slideUp(200);
			} else {
				$('.karCont').slideUp(200);
				$('#karCont-'+openKar).slideDown(200);
			}
			
		});

		
		// Smooth Scrolling
		$('a[href*=#]').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
			&& location.hostname == this.hostname) {
				var $target = $(this.hash);
				$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
				if ($target.length) {
					var targetOffset = $target.offset().top;
					$('html,body').stop().animate({scrollTop: targetOffset-60}, 2000);
					return false;
				}
			}
		});
		


		// Scrolling Nav

		$(document).scroll(function() {
			if($(window).scrollTop() > 220) {
				$('body').addClass('navan');
			} else {
				$('body').removeClass('navan');
			}
		});


		
		// Filialen
		$('.filialPOI').click(function(){
			
			var current = $(this);
			var currentID = current.attr('data-open');
			
			$('.filialPOI').removeClass('active');
			current.addClass('active');
			
			$('.filialContent').removeClass('active');
			$('.filialContent-'+currentID).addClass('active');
			
			$('.filialNote').fadeOut(300);
				
		});
	



		
		$('body').on('click', '#ytc', function(){
			
			$('#yto').remove();
				
		});
		
		
		// Tabs
		$('.wocheTabs-Button').click(function(){
		
			var current = $(this);
			var toOpen = current.attr('data-open');
			var parent = current.parents('.wocheTabs');
			
			parent.find('.wocheTabs-Button').removeClass('active');
			parent.find('.wocheTabs-Content').hide();
			
			current.addClass('active');
			parent.find('.wocheTabsContent-'+toOpen).show();
			
			$('.wocheSlider').trigger('updateSizes');
			$('#offersHint').fadeOut(300);
			
		});
		
		$('#offerNext').click(function(){
			$('#offerWeek').trigger('slideTo', 1);
		});
		
		$('#menuNext').click(function(){
			$('#menuWeek').trigger('slideTo', 1);
		});
		
		// Woche
		$('.wocheSlider').carouFredSel({
			responsive: true,
			circular: false,
			infinite: false,
			auto: false,
			align: "center",
			items: {
				visible: 1,
			},
			scroll: {
				items: 1,
				fx: "slide",
				duration: 300
			},
			prev: {
				button: function(){
					return $(this).parents('.wocheTabs-Content').find('.sliderPrev');
				}
			},
			next: {
				button: function(){
					return $(this).parents('.wocheTabs-Content').find('.sliderNext');
				}
			}
		});
		
		

		jQuery(window).load(function() {

			// Geschenke
			$('.geschenkeSlider').carouFredSel({
				responsive: true,
				circular: true,
				infinite: true,
				auto: false,
				align: "center",
				items: {
					visible: 1,
				},
				scroll: {
					items: 1,
					fx: "fade",
					duration: 500,
					onBefore: function(data){
						var height = data.items.new.height();
						$('.geschenkeSliderOuter').animate({
							'height': height
						}, 300);
					}
				},
				prev: { button: $('.geschenkePrev') },
				next: { button: $('.geschenkeNext') }
			});
			
			// Rezepte
			$('.rezeptSlider').carouFredSel({
				responsive: true,
				circular: false,
				infinite: false,
				auto: false,
				align: "center",
				items: {
					start: 1,
					visible: 1,
				},
				scroll: {
					items: 1,
					fx: "slide",
					duration: 300
				},
				prev: {
					button: function(){
						return $(this).parents('.contentModule').find('.sliderPrev');
					}
				},
				next: {
					button: function(){
						return $(this).parents('.contentModule').find('.sliderNext');
					}
				}
			});
			


			// Geschichte

			reverse = $( '.historySlider' ); // your parent ul element
			reverse.children().each( function( i, item ) { 
				reverse.prepend( item );
			} );

			
			jQuery( window ).resize( function() {

				var wwidth = jQuery( window ).width();

				if( wwidth < 600 ) {

					jQuery('.historySlider').trigger('configuration', {
						items: {
							visible: 1,
						},
						scroll: {
							items: 1,
							duration: 200
						},
					});

				} else {

					jQuery('.historySlider').trigger('configuration', {
						items: {
							visible: 3,
						},
						scroll: {
							items: 3,
							duration: 600
						},
					});

				}

			} );



			$('.historySlider').carouFredSel({
				responsive: true,
				circular: false,
				infinite: false,
				auto: false,
				align: "center",
				items: {
					visible: 3,
				},
				scroll: {
					items: 3,
					fx: "slide",
					duration: 600
				},
				prev: {
					button: function(){
						return $(this).parents('.historySliderContainer').find('.sliderPrev');
					}
				},
				next: {
					button: function(){
						return $(this).parents('.historySliderContainer').find('.sliderNext');
					}
				}
			});


			// Bildslider
			$('.imageSlider ul').carouFredSel({
				responsive: true,
				circular: true,
				infinite: true,
				auto: false,
				align: "center",
				items: {
					visible: 1,
				},
				scroll: {
					items: 1,
					fx: "crossfade",
					duration: 1000
				},
				prev: {
					button: function(){
						return $(this).parents('.ImagesliderContainer').find('.sliderPrev');
					}
				},
				next: {
					button: function(){
						return $(this).parents('.ImagesliderContainer').find('.sliderNext');
					}
				}
			});
			
			// Angebote
			$('.offersSlider').carouFredSel({
				responsive: true,
				circular: false,
				infinite: false,
				auto: false,
				align: "center",
				items: {
					visible: 1,
					width: '920px'
				},
				scroll: {
					items: 1,
					fx: "slide",
					duration: 300
				},
				prev: {
					button: function(){
						return $(this).parents('.offersSliderContainer').find('.sliderPrev');
					}
				},
				next: {
					button: function(){
						return $(this).parents('.offersSliderContainer').find('.sliderNext');
					}
				}
			});


		});


		
		setTimeout(function(){
			$(document).trigger('resize');
		},300);
		



		
	}); // End function
	
	
	
