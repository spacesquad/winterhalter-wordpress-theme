<?php get_header(); ?>

		<?php
			if (is_front_page()) {
				areaCreate();
			} else {
				get_template_part( 'template-parts/content', 'page' );
			}
		?>

<?php get_footer(); ?>
