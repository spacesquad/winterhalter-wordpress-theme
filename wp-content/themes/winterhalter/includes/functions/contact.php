<?php	

	
	// Contact function
	function contactform($options = '') {
				
		// Create options
        $defaults = array(
            'recipient' => get_bloginfo('admin_email'),
            'subject' => 'Webseiten-Kontakt',
			
			'settingsAction' => get_permalink(get_queried_object_id()),
			'settingsLabels' => true,
			'settingsPlaceholders' => true,
			'settingsAutocomplete' => false,
			'settingsMessage' => true,
			
			'messageDefault' => 'Geben Sie Ihre Nachricht ein, oder kontaktieren Sie uns direkt.',
			'messageSuccess' => 'Ihre Nachricht wurde versandt.',
			'messageError' => 'Es wurde ein Fehler an Ihren Eingaben festgestellt. Bitte überprüfen Sie diese.',
			
			'buttonText' => 'Senden',
			
			'labelName' => 'Name',
			'labelMail' => 'Mail-Adresse',
			'labelMessage' => 'Nachricht',
			
			'placeholderName' => 'Name',
			'placeholderMail' => 'Mail-Adresse',
			'placeholderMessage' => 'Nachricht',
			
			'errorEmptyName' => 'Geben Sie einen Namen an',
			'errorEmptyMail' => 'Geben Sie eine Mail-Adresse an',
			'errorWrongMail' => 'Geben Sie eine korrekte Mail-Adresse an',
			'errorEmptyMessage' => 'Geben Sie eine Nachricht ein',
			
			'classesBlock' => 'formBlock',
			'classesLabel' => 'formLabel',
			'classesButton' => 'formButton formFluid',
			'classesInput' => 'formText formFluid',
			'classesIntro' => 'pb',
			'classesArea' => 'formArea formFluid',
			'classesErrorText' => 'formError',
			'classesSuccessText' => 'formSuccess',
			'classesErrorBlock' => '',
			'classesForm' => '',
			'classesClear' => 'clear',
			
        );
        
        foreach($defaults as $key => $default) {
            $options[$key] = isset($options[$key]) ? $options[$key] : $default;
        }
		
		
		// If post check data
		if($_SERVER['REQUEST_METHOD'] == 'POST'):
		
			// Get post data
			$nic = $_POST[contactname];
			$mail = $_POST[contactmail];
			$text = $_POST[contacttext];
			
			// Errors
			$failure = false;
			$emptynamefailure = false;
			$emptytextfailure = false;
			$emptymailfailure = false;
			$wrongmailfailure = false;
			$contactsuccess = false;
			
			// Check form data
			if(empty($nic)) { $failure = true; $emptynamefailure = true; }
			if(empty($text)) { $failure = true; $emptytextfailure = true; }
			if(empty($mail)) { $failure = true; $emptymailfailure = true; }
			if(!preg_match('/^[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+(?:\.[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+)*\@[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+(?:\.[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+)+$/i', $mail)) { $failure = true; $wrongmailfailure = true; }
			
			// Mail content
			$from = "From: $nic <$mail>";
			$request = "$text";
			
			// If ok send mail
			if(!$failure) {
				mail($options['recipient'], $options['recipient'], $request, $from);
				$contactsuccess = true;
			}
		
		endif;
		
		
		if($options['settingsMessage']):
			
			echo '<div class="' . $options['classesIntro'] . '">';
				
				if($contactsuccess):
					echo '<span class="'.$options['classesSuccessText'].'">' . $options['messageSuccess'] . '</span>';
				elseif($failure):
					echo '<span class="'.$options['classesErrorText'].'">' . $options['messageError'] . '</span>';
				else:
					echo $options['messageDefault'];
				endif;
				
			echo '</div>';
			
		endif;
		
		// Throw out the form
		echo '<form action="' . $options['settingsAction'] . '" method="post" class="' . $options['classesForm'] . '">';
		
			
			$blockClasses = $options['classesBlock'];
			if($emptynamefailure) $blockClasses .= ' '.$options['classesErrorBlock'];
			echo '<div class="' . $blockClasses . '">';
			
			if($options['settingsLabels']):
				echo '<div class="' . $options['classesLabel'] . '">';
				echo $emptynamefailure ? '<span class="'.$options['classesErrorText'].'">'.$options['errorEmptyName'].'</span>' : $options['labelName'];
				echo '</div>';
			endif;
			
			$inputMeta = 'class="' . $options['classesInput'] . '"';
			if( $options['settingsPlaceholders'] ) $inputMeta .= ' placeholder="' . $options['placeholderName']. '"';
			if( !$options['settingsAutocomplete'] ) $inputMeta .= ' autocomplete="off"';
			if( $contactsuccess ) $inputMeta .= ' disabled="disabled"';
			echo '<input name="contactname" type="text" value="' . $nic . '" ' . $inputMeta . ' />';
			
			echo '</div>';
			
			#######
			
			$blockClasses = $options['classesBlock'];
			if($emptymailfailure OR $wrongmailfailure) $blockClasses .= ' '.$options['classesErrorBlock'];
			echo '<div class="' . $blockClasses . '">';
			
			if($options['settingsLabels']):
				echo '<div class="' . $options['classesLabel'] . '">';
				if($emptymailfailure):
					echo '<span class="'.$options['classesErrorText'].'">'.$options['errorEmptyMail'].'</span>';
				elseif($wrongmailfailure):
					echo '<span class="'.$options['classesErrorText'].'">'.$options['errorWrongMail'].'</span>';
				else:
					echo $options['labelMail'];
				endif;
				echo '</div>';
			endif;
			
			$inputMeta = 'class="' . $options['classesInput'] . '"';
			if( $options['settingsPlaceholders'] ) $inputMeta .= ' placeholder="' . $options['placeholderMail']. '"';
			if( !$options['settingsAutocomplete'] ) $inputMeta .= ' autocomplete="off"';
			if( $contactsuccess ) $inputMeta .= ' disabled="disabled"';
			echo '<input name="contactmail" type="text" value="' . $mail . '" ' . $inputMeta . ' />';
			
			echo '</div>';
			
			#######
			
			$blockClasses = $options['classesBlock'];
			if($emptytextfailure) $blockClasses .= ' '.$options['classesErrorBlock'];
			echo '<div class="' . $blockClasses . '">';
			
			if($options['settingsLabels']):
				echo '<div class="' . $options['classesLabel'] . '">';
				echo $emptytextfailure ? '<span class="'.$options['classesErrorText'].'">'.$options['errorEmptyMessage'].'</span>' : $options['labelMessage'];
				echo '</div>';
			endif;
			
			$inputMeta = 'class="' . $options['classesInput'] . '"';
			if( $options['settingsPlaceholders'] ) $inputMeta .= ' placeholder="' . $options['placeholderMessage']. '"';
			if( !$options['settingsAutocomplete'] ) $inputMeta .= ' autocomplete="off"';
			if( $contactsuccess ) $inputMeta .= ' disabled="disabled"';
			echo '<textarea name="contacttext" rows="5" cols="30" style="resize: vertical;" ' . $inputMeta . ' >' . $text . '</textarea>';
			
			echo '</div>';
			
			#######
			
			echo '<div class="' . $options['classesBlock'] . '">';
			
			echo '<input type="submit" class="' . $options['classesButton'] . '" value="' . $options['buttonText'] . '" />';
			
			echo '</div>';
		
		
		
		echo '</form>';
		
	}
		
	

   