<?php add_action( 'wp_footer', function() { ?>

	
		<?php if( $_COOKIE[ 'cookielaw' ] != 'hide' ): ?>


			<div id="cookielaw_container">
				<div id="cookielaw_holder">
					<div id="cookielaw_window">
						
						<div id="cookielaw_content">
						
							<div onclick="cookielaw_close();" id="cookielaw_close">&#215;</div>

							<h1>Cookies</h1>

							<p><?php _e( 'Diese Webseite verwendet Cookies, um korrekt zu funktionieren und Ihnen ein optimales Nutzererlebnis zu bieten. Wenn Sie damit einverstanden sind, setzen Sie Ihren Besuch der Seite fort, oder sehen Sie sich für weitere Informationen unser Impressum an.', 'Theme' ); ?></p>
							
							<button onclick="cookielaw_close();" class="formButton form_button"><?php _e( 'Besuch fortsetzen', 'Theme' ); ?></button>
							
							<a onclick="cookielaw_close();" href="<?php echo get_the_permalink( get_page_by_title( 'Impressum' ) ); ?>"><?php _e( 'Weitere Informationen', 'Theme' ); ?></a>

						</div>

					</div>
				</div>
			</div>

			<style>
				
				#cookielaw_container {
					background: rgba(0,0,0,0.6);
				    display: table;
				    position: fixed;
				    top: 0px;
				    left: 0px;
				    right: 0px;
				    bottom: 0px;
				    width: 100%;
				    height: 100%;
				    z-index: 999999999;
				    cursor: not-allowed;
				}

				#cookielaw_holder {
				    display: table-cell;
				    vertical-align: middle;
				    padding: 20px;
				}

				#cookielaw_close {
					width: 50px;
					height: 50px;
					line-height: 50px;
					text-align: center;
					font-size: 30px;
					position: absolute;
					top: 10px;
					right: 10px;
					cursor: pointer;
					color: #aaa;
				}

				#cookielaw_close:hover {
					color: #333;
				}

				#cookielaw_window {
					position: relative;
				    margin-left: auto;
				    margin-right: auto; 
				    max-width: 400px;
				    background: #fff;
				    box-shadow: 0px 0px 20px rgba(0,0,0,0.2);
				    overflow: auto;
				    transition: all 0.1s ease-in-out;
				    cursor: default;
				}

				#cookielaw_content {
					padding: 40px 40px 30px 40px;
				}

				#cookielaw_content p {
				    margin: 20px 0px;
				}

				#cookielaw_content a {
				    display: block;
				    text-align: center;
				    margin-top: 15px;
				}

				#cookielaw_content button {
				    width: 100%;
				}

				#cookielaw_container:active #cookielaw_window  {
					transform: scale(1.03);
				}

				#cookielaw_container:active #cookielaw_window:active  {
					transform: scale(1.0);
				}

				@media only screen and (max-width: 450px) {
					
					#cookielaw_content {
					    padding: 25px 25px 20px 25px;
					}

				}

			</style>

			<script>

				var cookielaw = 1;


				// Close window

				function cookielaw_close() {

					var today = new Date();
					var expire = new Date();
					var element = document.getElementById( 'cookielaw_container' );

					expire.setTime(today.getTime() + 3600000*24*365);
					document.cookie = "cookielaw=hide;expires="+expire.toGMTString();
					
					element.parentNode.removeChild(element);

					cookielaw = null;

				}

				// Resizing

				function overlaySize() {

					var height = window.innerHeight - 80;
					var overlay = document.getElementById('cookielaw_window');

					overlay.style.height = '';

					if( overlay.offsetHeight > height ) {

						overlay.style.height = height + 'px';

					}

				}

				window.onresize = function(event) {

					overlaySize();

				}; overlaySize();

			</script>


		<?php endif; ?>


<?php } ); ?>