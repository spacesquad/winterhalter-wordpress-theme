<div class="career">

	<div class="tt tbrown pth">Karriere</div>

	<table>

		<tr class="topRow">

			<td>Offene Stellen</td>
			<td>Eintrittsdatum</td>
			<td>Standort</td>

		</tr>

	</table>


	<div>

		<?php rewind_posts();
		global $more;
		$my_query = new WP_Query('cat=4&posts_per_page=-1');
		while ($my_query->have_posts()) : $my_query->the_post(); ?>


			<table>
				<tr class="karRow">

					<td class="karTit" data-open="<?php the_ID(); ?>">
						<?php the_title(); ?>
					</td>

					<td>
						<?php the_field('date'); ?>
					</td>

					<td>
						<?php the_field('location'); ?>
					</td>

				</tr>

			</table>


			<div id="karCont-<?php the_ID(); ?>" class="karCont">
				<?php the_content(); ?>

				<div class="karGo fix">

					<strong>Warte nicht länger und bewerbe Dich bei uns</strong>
					<br/>
					<br/>
					Entweder per E-mail unter info@metzgerei-winterhalter.de<br/><br/>
					oder auf dem Postweg unter:<br/>
					Metzgerei Gustav Winterhalter GmbH<br/>
					Herrn Thomas Winterhalter<br/>
					Telferstr. 4, 79215 Elzach

				</div>

			</div>


		<?php endwhile;
		wp_reset_query(); ?>

	</div>

</div>



