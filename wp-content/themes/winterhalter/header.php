<!DOCTYPE html><html <?php language_attributes(); ?>><head>

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

            <title><?php bloginfo('name'); ?> | <?php if(is_front_page()) bloginfo('description'); else wp_title(''); ?></title>

           	<meta name="viewport" content="width=device-width,  initial-scale=1 maximum-scale=1, user-scalable=0" />

            <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css?20221124" type="text/css" media="screen" />

            <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/apple-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/favicon-16x16.png">
            <link rel="manifest" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/manifest.json">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">

            <meta http-equiv="X-UA-Compatible" content="IE=edge" />

            <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/scripts/jquery.js"></script>
            <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/scripts/caroufredsel.js"></script>
            <script type="text/javascript" async src="<?php bloginfo('template_url'); ?>/assets/scripts/functions.js"></script>

            <script type="text/javascript">

                var gaProperty = 'UA-17956607-33';
                var disableStr = 'ga-disable-' + gaProperty;

                if (document.cookie.indexOf(disableStr + '=true') > -1) {
                window[disableStr] = true;
                }
                function gaOptout() {
                document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
                window[disableStr] = true;
                alert( 'Die Erfassung Ihrer Daten wird in Zukunft verhindert.');
                }

            </script>

			<script>

				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				ga('create', 'UA-17956607-33', 'auto');
				ga('set', 'anonymizeIp', true);
				ga('send', 'pageview');

            </script>

            <?php wp_head(); ?>

        </head>

		<body id="start" <?php body_class(); ?>>


            <a id="logo" href="<?php echo home_url(); ?>">

            <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/logo.png" loading="eager" width="295" height="250" />

            </a>


            <div id="nav">

                <div class="floater fix">



                    <div class="left">

                        <a id="sublogo" href="#start">

                            <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/sublogo.svg" width="140px" height="10" viewBox="0 0 140 10" />

                        </a><!-- #sublogo -->

                    </div><!-- .left -->

                    <div class="right">

                        <?php wp_nav_menu(array('theme_location' => 'header', 'container' => false)); ?>


                        <a id="shop_button" href="https://shop.metzgerei-winterhalter.de/" target="_blank">Onlineshop</a>


                    </div><!-- .right -->

                </div>

            </div><!-- #nav -->






            <div id="showcase">

                <div id="showcase-Person">

                    <?php

                        $persons = glob( get_stylesheet_directory() . '/assets/images/persons/*.png' ); shuffle( $persons );

                    ?>

                    <img src="<?php echo get_bloginfo('template_url') . str_replace( get_stylesheet_directory(), '', $persons[0] ); ?>" alt="">

                </div>

            	<div id="showcase-Content">



                	<h1>Bestes<br>aus unserer<br>Heimat</h1>



                    <p>
                    	Enge Zusammenarbeit mit den hiesigen Landwirten, <br>Schlachtung im eigenen Hause und Zulieferung <br>regionaler Zutaten garantieren absolute Frische, <br>gesunde Inhalte und ist ökonomisch &hellip;
                    </p>




                    <?php if( $b1url = get_field( 'button_1_url', 'option' ) AND $b1txt = get_field( 'button_1_txt', 'option' ) ): ?>

                        <a class="showVideo bright button" href="<?php echo $b1url; ?>" target="_blank"><?php echo $b1txt; ?></a>

                    <?php else: ?>

                        <a class="openoverlay-video showVideo bright button">Die Metzgerei im Überblick</a>

                    <?php endif; ?>



                    <br /><br />



                    <?php if( $b2url = get_field( 'button_2_url', 'option' ) AND $b2txt = get_field( 'button_2_txt', 'option' ) ): ?>

                        <a class="showVideo bright button" href="<?php echo $b2url; ?>" target="_blank"><?php echo $b2txt; ?></a>

                    <?php else: ?>

						<!-- TODO: Richtigen Link vermerken, sobald der Serverumzug durch ist -->
                        <a href="/?page_id=939" class="bright button">Karriere bei Winterhalter</a>

                    <?php endif; ?>



                    <br />



                    <a id="headerLink" href="<?php the_field( 'lmiv_link', 'option' ); ?>" target="blank">Zu den LMIV-Produktspezifikationen</a>



                </div>







            </div><!-- #showcase -->



            <div id="content">
                <div id="contentinner">


                <a id="shop_teaser" href="https://shop.metzgerei-winterhalter.de/" target="_blank">
                    <small>Hier geht’s zum</small>
                    Online
                    Shop
                </a>


				<?php if (is_front_page()) : ?>
                <div class="contentModule modpad fix bgPaper">


                    <div class="cs cs3-111 fix">

                        <div class="cc">

                            <h1 class="tgreen">News</h1>

                        </div>


                        <div class="cc">

                            <?php contentCreate( 3, 'image', '250xauto' ); ?>

                            <?php contentCreate( 1, 'text'); ?>

                        </div>

                        <div class="cc">

                            <?php contentCreate( 4, 'image', '250xauto' ); ?>

                            <?php contentCreate( 2, 'text'); ?>

                        </div>

                    </div>

                </div><!-- .contentModule -->
				<?php endif; ?>


