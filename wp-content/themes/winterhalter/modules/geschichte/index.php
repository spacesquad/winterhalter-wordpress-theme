	

	<div id="geschichte" class="contentModule contentModule-History colored_arrows clearfix">	
    	
        <div class="historyShowcase sht shb">
        
			<?php contentCreate( 1, 'image', '1840xauto'); ?>
            
            <div class="historyContent">
            
            	<h1><?php contentCreate( 2, 'text'); ?></h1>
                
                <div class="subContent"><?php contentCreate( 3, 'text'); ?></div>
                
            </div>
            
            <div class="historyPolaroids"></div>
                        
        </div><!-- .historyShowcase -->
       
        <div class="historySliderContainer clearfix rel modpad">
        	
            <div class="tt tbrown"><?php contentCreate( 4, 'text'); ?></div>
            
            <div class="historySlider">

            	<?php $c = optionGet('slides'); if(!empty($c)): $t = 4; for ($i = 1; $i <= $c; $i++): ?>
            			
                    <div class="historySlide">
                        
                        <strong><?php contentCreate('['.++$t.']', 'text'); ?></strong>
                        <div class="subContent">
                            <?php contentCreate('['.++$t.']', 'text'); ?>
                        </div>
                        
                    </div>
                    
                <?php endfor; endif; ?>
            
            </div>
            
            <div class="sliderPrev"></div>
       		<div class="sliderNext"></div>
            
        </div>
       
        
       
    </div>