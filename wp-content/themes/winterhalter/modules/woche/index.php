	
	<div id="woche" class="contentModule contentModule-Woche colored_arrows rel clearfix modpad">	


      <!-- <div id="metalogos">
                    
          <img src="<?php bloginfo('template_url'); ?>/assets/images/wh-logos-akdh.png" />

          <img src="<?php bloginfo('template_url'); ?>/assets/images/wh-logos-ecarf.png" />

      </div><!-- #metalogos -->

                
    	
        <div class="pol abs"><img src="<?php bloginfo('template_url'); ?>/assets/images/pol1.jpg" alt="" /></div>
        <div class="tip abs"></div>
        
        <div class="wocheTabs clearfix">
                
            <h1 class="wocheTabs-Button tgreen active" data-open="menu">Menüplan</h1><h1 class="wocheTabs-Button tgreen" data-open="offers">Angebote<span id="offersHint"></span></h1>
        	
            <div class="wocheTabsContent-menu wocheTabs-Content">
            
            	<div class="wocheSlider" id="menuWeek">
                
                	<div class="wocheSlide fix">
                    	

                      <strong class="tt tbrown"><?php contentCreate('[0]', 'text'); ?></strong><br /><br />


                      <div class="wocheMain left">
                          
                          <div class="wocheMainInner">
                          
                              <table class="wocheTable">
                                <tr>
                                  <td class="wocheKurz">MO</td>
                                  <td><?php contentCreate( 1, 'text'); ?></td>
                                  <td class="wochePreis"><?php contentCreate( 2, 'text'); ?></td>
                                </tr>
                                <tr>
                                  <td class="wocheKurz">DI</td>
                                  <td><?php contentCreate( 3, 'text'); ?></td>
                                  <td class="wochePreis"><?php contentCreate( 4, 'text'); ?></td>
                                </tr>
                                <tr>
                                  <td class="wocheKurz">MI</td>
                                  <td><?php contentCreate( 5, 'text'); ?></td>
                                  <td class="wochePreis"><?php contentCreate( 6, 'text'); ?></td>
                                </tr>
                                <tr>
                                  <td class="wocheKurz">DO</td>
                                  <td><?php contentCreate( 7, 'text'); ?></td>
                                  <td class="wochePreis"><?php contentCreate( 8, 'text'); ?></td>
                                </tr>
                                <tr>
                                  <td class="wocheKurz">FR</td>
                                  <td><?php contentCreate( 9, 'text'); ?></td>
                                  <td class="wochePreis"><?php contentCreate( 10, 'text'); ?></td>
                                </tr>
                              </table>
                          
                          </div><!-- .pr -->

                        </div><!-- .left -->
                        
                        <div class="wocheAside rel left">
                        	
                            <div class="hitderwoche">
                            
                                <strong class="tt tgreen">Hit der Woche</strong>
                                <br /><br />
                                <div>
                                    <div style="float: left; width: 150px;"><?php contentCreate( 11, 'text'); ?></div>
                                    <div class="wochePreis" style="float: right; width: 80px;"><?php contentCreate( 12, 'text'); ?></div>
                                    <div class="clear"></div>
                                </div>
                                
                            </div>

                            
                            <strong class="tt tgreen">Wochenvorschau</strong>
                            
                            <div class="subContent">
                                Sehen Sie sich unseren Menüplan der nächsten Woche an.
                            </div>
                            
                            <button id="menuNext">Ansehen</button>
                            
                            
                            
                        </div><!-- .wocheAside -->
                        
                    </div><!-- .wocheSlide -->
                    
                    <div class="wocheSlide fix">
                    	
                        <strong class="tt tbrown"><?php contentCreate( 13, 'text'); ?></strong><br /><br />
                        
                        <div class="wocheMain left">
                          
                          <div class="wocheMainInner">

                        <div class="wocheLeft">
                        
                            <table class="wocheTable">
                              <tr>
                                <td class="wocheKurz">MO</td>
                                <td><?php contentCreate( 14, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 15, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td class="wocheKurz">DI</td>
                                <td><?php contentCreate( 16, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 17, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td class="wocheKurz">MI</td>
                                <td><?php contentCreate( 18, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 19, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td class="wocheKurz">DO</td>
                                <td><?php contentCreate( 20, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 21, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td class="wocheKurz">FR</td>
                                <td><?php contentCreate( 22, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 23, 'text'); ?></td>
                              </tr>
                            </table>
                        
                        </div><!-- .wocheLeft -->
                        
                        </div></div>

                        <div class="wocheAside rel left">
                            
                            <div class="hitderwoche">
                            
                                <strong class="tt tgreen">Hit der Woche</strong>
                                <br /><br />
                                <div>
                                	<div style="float: left; width: 150px;"><?php contentCreate( 24, 'text'); ?></div>
                                    <div class="wochePreis" style="float: right; width: 80px;"><?php contentCreate( 25, 'text'); ?></div>
                                    <div class="clear"></div>
                                </div>
                                
                            </div>
                            
                        </div><!-- .wocheAside -->
                        
                        

                    </div><!-- .wocheSlide -->
                
                </div><!-- .wocheSlider -->
            	
                <div class="clear"></div>
                
                <div class="sliderPrev"></div>
        		<div class="sliderNext"></div>
                
            </div><!-- .wocheTabs-Content  -->
            
            <div class="wocheTabsContent-offers wocheTabs-Content" style="display: none;">
            
            	<div class="wocheSlider" id="offerWeek">
                
                	<div class="wocheSlide fix">
                    	
                        <strong class="tt tbrown"><?php contentCreate( 26, 'text'); ?></strong><br /><br />
                        
                        <div class="wocheMain left">
                          
                          <div class="wocheMainInner">

                        <div class="wocheLeft">
                        
                            <table class="wocheTable">
                              <tr>
                                <td><?php contentCreate( 27, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 28, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td><?php contentCreate( 29, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 30, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td><?php contentCreate( 31, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 32, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td><?php contentCreate( 33, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 34, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td><?php contentCreate( 35, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 36, 'text'); ?></td>
                              </tr>
                            </table>
                        	
                            <div style="color: #999;">* Käseangebote erhältlich in Elzach, Freiburg, Waldkirch, Sexau und Schönwald</div>
                            
                        </div><!-- .wocheLeft -->
                        
                        </div></div>

                        <div class="wocheAside rel left">
                        
                            <strong class="tt tgreen">Wochenvorschau</strong>
                            
                            <div class="subContent">
                                Sehen Sie sich unsere Angebote der nächsten Woche an.
                            </div>
                            
                            <button id="offerNext">Ansehen</button>
                            
                        </div><!-- .wocheAside -->
                        
                        

                    </div><!-- .wocheSlide -->
                    
                    <div class="wocheSlide fix">
                    	
                        <strong class="tt tbrown"><?php contentCreate( 37, 'text'); ?></strong><br /><br />
                        
                        <div class="wocheMain left">
                          
                          <div class="wocheMainInner">

                        <div class="wocheLeft">
                        
                            <table class="wocheTable">
                              <tr>
                                <td><?php contentCreate( 38, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 39, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td><?php contentCreate( 40, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 41, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td><?php contentCreate( 42, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 43, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td><?php contentCreate( 44, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 45, 'text'); ?></td>
                              </tr>
                              <tr>
                                <td><?php contentCreate( 46, 'text'); ?></td>
                                <td class="wochePreis"><?php contentCreate( 47, 'text'); ?></td>
                              </tr>
                            </table>
                        	
                            <div style="color: #999;">* Käseangebote erhältlich in Elzach, Freiburg, Waldkirch, Sexau und Schönwald</div>
                            
                        </div><!-- .wocheLeft -->
                        
                        </div></div>

                        
                        <div class="wocheAside rel left">
                            
                            
                        </div><!-- .wocheAside -->

                        
                        
                    </div><!-- .wocheSlide -->
                
                </div><!-- .wocheSlider -->
            	
                <div class="clear"></div>
                
                <div class="sliderPrev"></div>
        		<div class="sliderNext"></div>
            
            </div><!-- .wocheTabs-Content  -->
            <div class="clear"></div>
                        
        </div><!-- .wocheTabs -->
        <div class="clear"></div>
        
    </div>