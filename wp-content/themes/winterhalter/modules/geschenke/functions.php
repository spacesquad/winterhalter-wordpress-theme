<?php

	
	add_action( 'gdymc_module_options_settings', function ( $module ) {
	    if( $module->type == gdymc_module_name( __FILE__ ) ):
	    	
		    optionInput( 'slides', array(
			
				'type' => 'number',
				'default' => 1,
				'placeholder' => 3,
				'label' => __( 'Anzahl Slides', 'Theme' ),
			
			), $module->id );

	    endif;
	} );

	
	
?>