    
	<div id="geschenke" class="contentModule contentModule-Geschenke rel clearfix modpad">	
    	
        <div class="tip"></div>
        
        <h1><?php contentCreate( 1, 'text'); ?></h1>
       	
        <div class="geschenkeContainer">

            <div class="geschenkeAside twhite">
    			<?php contentCreate( 2, 'text'); ?>
                <a class="button bright" href="#filialen">Zum Kontakt</a>
            </div>
            
            <div class="geschenkeSliderOuter">
            
                <div class="geschenkeSlider">
                	
                    <?php $c = optionGet('slides'); if(!empty($c)): $t = 2; for ($i = 1; $i <= $c; $i++): ?>
                        
                        <div class="geschenkeSlide fix">
                            
                            <div class="geschenkeDesc">
                                
                                <div class="geschenkeHead tt"><?php contentCreate('['.++$t.']', 'text'); ?></div>
                                <div class="subLine ts"><?php contentCreate('['.++$t.']', 'text'); ?></div>
                                <div class="subContent"><?php contentCreate('['.++$t.']', 'text'); ?></div>
                                
                                <div class="geschenkePreisContainer">
                                    <?php contentCreate('['.++$t.']', 'text'); ?><span class="geschenkePreis"><?php contentCreate('['.++$t.']', 'text'); ?></span>
                                </div>
                                
                            </div>

                            <div class="geschenkeImg"><?php contentCreate('['.++$t.']', 'image', '249xauto'); ?></div>
                            
                        </div><!-- .geschenkeSlide -->
                        
                    <?php endfor; endif; ?>
                            
                </div><!-- .geschenkeSlider -->
            
            </div><!-- .geschenkeSliderOuter -->

        </div>
        
        <div class="sliderPrev geschenkePrev"></div>
        <div class="sliderNext geschenkeNext"></div>
        
    </div>