
	<div id="rezepte" class="contentModule contentModule-Rezepte colored_arrows rel clearfix">

        <div class="pol abs pol1"><?php contentCreate( 0, 'image', '200x200'); ?></div>
        <div class="pol abs pol2"><img src="<?php bloginfo('template_url'); ?>/assets/images/pol2.jpg" loading="lazy" alt="" /></div>
        <div class="pol abs pol3"><img src="<?php bloginfo('template_url'); ?>/assets/images/pol3.jpg" loading="lazy" alt="" /></div>

        <div class="rezeptSlider">

			<div class="rezeptSlide modpad">

                <div class="tt tbrown strong"><?php contentCreate( 1, 'text'); ?></div>

                <h1 class="tgreen"><?php contentCreate( 2, 'text'); ?></h1>

                <div class="clearfix">

                    <div class="rezeptInhalte">
                    	<?php contentCreate( 3, 'text'); ?>
                    </div>

                    <div class="rezeptBeschreibung">
                        <div class="pl">
                    	   <?php contentCreate( 4, 'text'); ?>
                        </div>
                    </div>

                </div>

            </div>

            <div class="rezeptSlide modpad">
            	<div>

                    <div class="tt tbrown strong"><?php contentCreate( 5, 'text'); ?></div>

                    <h1 class="tgreen"><?php contentCreate( 6, 'text'); ?></h1>

                    <div class="clearfix">

                        <div class="rezeptInhalte">
                            <?php contentCreate( 7, 'text'); ?>
                        </div>

                        <div class="rezeptBeschreibung">
                            <div class="pl">
                                <?php contentCreate( 8, 'text'); ?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

    	</div><!-- .rezeptSlider -->

        <div class="sliderPrev"></div>
        <div class="sliderNext"></div>

    </div>
