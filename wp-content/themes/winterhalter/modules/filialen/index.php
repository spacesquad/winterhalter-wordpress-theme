

	<div id="filialen" class="contentModule contentModule-Filialen">	
        
        <div id="filialKarte">
            
            <div data-open="2" class="filialPOI" style="left: 62px; top: 294px;"></div>
            <div data-open="3" class="filialPOI" style="left: 65px; top: 228px;"></div>
            <div data-open="4" class="filialPOI" style="left: 40px; top: 220px;"></div>
            <div data-open="5" class="filialPOI" style="left: 52px; top: 209px;"></div>
            <div data-open="6" class="filialPOI" style="left: 112px; top: 238px;"></div>
            <div data-open="7" class="filialPOI" style="left: 220px; top: 232px;"></div>
            <div data-open="8" class="filialPOI" style="left: 75px; top: 62px;"></div>
            <div data-open="9" class="filialPOI" style="left: 135px; top: 29px;"></div>
            <div data-open="10" class="filialPOI active" style="left: 215px; top: 188px;"></div>
            <div data-open="11" class="filialPOI" style="left: 224px; top: 178px;"></div>
            <div data-open="12" class="filialPOI" style="left: 62px; top: 115px;"></div>
            
            
            <div class="filialORT" style="left: 87px; top: 52px;">Lahr</div>
            <div class="filialORT" style="left: 150px; top: 21px;">Gengenbach</div>
            <div class="filialORT" style="left: 175px; top: 171px;">Elzach</div>
            <div class="filialORT" style="left: 63px; top: 200px;">Emmendingen</div>
            <div class="filialORT" style="left: 123px; top: 229px;">Waldkirch</div>
            <div class="filialORT" style="left: 231px; top: 223px;">Schönwald</div>
            <div class="filialORT" style="left: 74px; top: 285px;">Freiburg</div>
            <div class="filialORT" style="left: 47px; top: 236px;">Sexau</div>
            <div class="filialORT" style="left: 74px; top: 107px;">Ettenheim</div>
            
             <div class="filialNote"></div>
             
        </div>
		
        
       	
        <div class="filialContentContainer">

            <h1 class="tgreen">Kontakt</h1>
        	
            <?php $t = 0; for ($i = 1; $i <= 12; $i++): ?>
            			
                <div class="filialContent filialContent-<?php echo $i; ?><?php if($i == 10) echo ' active'; ?>">
                    <div class="abs pol" style="left: -130px; top: -40px; <?php rotation(); ?>"></div>
                    <div class="abs pol" style="left: -130px; top: -37px; <?php rotation(); ?>"><?php contentCreate('['.++$t.']', 'image', '150x150'); ?></div>
                    <div class="strong tt tgreen"><?php contentCreate('['.++$t.']', 'text'); ?></div>
                    <div style="margin: 10px 0px;"><?php contentCreate('['.++$t.']', 'text'); ?></div>
                    <table style="width: 100%; border: none;">
                        <tr>
                            <th><?php contentCreate('['.++$t.']', 'text'); ?></th>
                            <th><?php contentCreate('['.++$t.']', 'text'); ?></th>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><?php contentCreate('['.++$t.']', 'text'); ?></td>
                            <td style="vertical-align: top;"><?php contentCreate('['.++$t.']', 'text'); ?></td>
                        </tr>
                    </table>
                </div>
                    

                
            <?php endfor; ?>
            
        </div>
        
    </div>