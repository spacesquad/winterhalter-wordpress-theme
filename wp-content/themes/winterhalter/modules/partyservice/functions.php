<?php

	
	add_action( 'gdymc_module_options_settings', function ( $module ) {
	    if( $module->type == gdymc_module_name( __FILE__ ) ):
	    	
		    optionInput( 'download_url', array(
			
				'type' => 'text',
				'default' => '',
				'placeholder' => 'https://www.fouadvollmer.de/',
				'label' => __( 'Download-URL', 'Theme' ),
			
			), $module->id );

	    endif;
	} );

	
	
?>