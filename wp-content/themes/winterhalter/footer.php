	</div><!-- #contentinner -->
	</div><!-- #content -->

	<?php get_template_part('includes/content/instagram'); ?>

	<div class="site-footer" id="footer">

		<div class="left">

			<a class="social_button social_button_instagram" href="https://www.instagram.com/gustavwinterhalter/"
			   target="_blank"><i class="fab fa-instagram"></i></a>
			<a class="social_button social_button_facebook" href="https://www.facebook.com/GustavWinterhalter/"
			   target="_blank"><i class="fab fa-facebook-f"></i></a>

			<div class="footer-menu">
				<?php wp_nav_menu(array('theme_location' => 'footer', 'container' => false)); ?>
			</div>

		</div>

		<div class="right">
			<a href="https://www.fouadvollmer.de/" target="_blank">Webseite von <strong>Fouad Vollmer</strong></a>
		</div>

	</div><!-- #footer -->

	<!-- Video-Overlay: Die Metzgerei im Überblick -->
	<div id="overlay-video" class="overlay">
		<div class="overlaywindow">
			<div class="overlayclose"></div>
			<div class="overlayinner">
				<div class="ytv"><iframe width="1600" height="900" src="https://www.youtube-nocookie.com/embed/kf9HNe8juHs?rel=0&autoplay=0" frameborder="0" allowfullscreen></iframe></div>
			</div>
		</div>
	</div>

	<div class="c-modal c-modal--image" id="christmas-2022">
		<div class="c-modal__container">
			<div class="c-modal__close"></div>
			<a href="https://metzgerei-winterhalter.de/wp-content/uploads/2022/11/metzgerei-winterhalter-flyer-weihnachten-2022.pdf" target="_blank" rel="noopener">
				<img src="https://metzgerei-winterhalter.de/wp-content/uploads/2022/11/metzgerei-winterhalter-flyer-weihnachten-overlay.png" alt="Metzgerei Winterhalter Weihnachtsflyer 2022" />
			</a>
		</div>
	</div>

	<script>
		// Show c-modal only if cookie is not set
		if (document.cookie.indexOf('metzgerei-winterhalter-flyer-weihnachten-2022') === -1) {
			document.querySelector('#christmas-2022.c-modal').classList.add('is-active');
		}
		// Set cookie on modal close
		document.querySelector('#christmas-2022 .c-modal__close').addEventListener('click', function() {
			const date = new Date();
			date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
			document.cookie = `metzgerei-winterhalter-flyer-weihnachten-2022=1; expires=${date.toUTCString()}; path=/`;
			document.querySelector('#christmas-2022.c-modal').classList.remove('is-active');
		});
	</script>

	<?php wp_footer(); ?>

	<script>

	</script>

	</body>
</html>
