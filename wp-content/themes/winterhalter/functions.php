<?php


	/**************************** CHANGE THE GDYMC MODULE FOLDER ****************************/

	add_filter( 'gdymc_modules_folder', 'my_callback' );

    function my_callback( $content ) {

        return get_template_directory() . '/modules';

    }


    /**************************** ACF OPTIONS PAGE ****************************/

	if( function_exists( 'acf_add_options_page' ) ) {

		acf_add_options_page(array(
			'page_title' 	=> 'Optionen',
			'menu_title'	=> 'Optionen',
			'menu_slug' 	=> 'theme-options',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));

	}


	// Navigation registrieren
	if(function_exists('register_nav_menu')) {
		register_nav_menu('header', 'Hauptmenü');
		register_nav_menu('footer', 'Footer-Menü');
	}

	// Polaroid Drehung
	function rotation(){
		$r = rand(-15, 15);
		echo 'transform: rotate('.$r.'deg); -webkit-transform: rotate('.$r.'deg); -moz-transform: rotate('.$r.'deg); -o-transform: rotate('.$r.'deg); -ms-transform: rotate('.$r.'deg);';
	}

	// Tinymce style
	add_editor_style('/styles/tinymce.css');

/**
 * Wandelt YouTube-oEmbeds in No-Cookie oEmbeds um
 *
 * @param $html
 * @param $url
 * @return string
 */
function youtube_nocookie_oembed($html, $url)
{
	if (!empty($url) && strpos($url,'youtube.com') !== false) {
		$html = str_replace('youtube.com', 'youtube-nocookie.com', $html);
	}
	return $html;
}
add_filter( 'embed_oembed_html', 'youtube_nocookie_oembed', 10, 2 );

/**
 * Shortcut zum einfügen der Jobangebote
 *
 * @param $atts
 * @param null $content
 * @return false|string
 */
function karriere_shortcut( $atts, $content = null ) {
	ob_start();
	include 'includes/content/karriere.php';
	return ob_get_clean();
}
add_shortcode( 'karriere', 'karriere_shortcut' );
