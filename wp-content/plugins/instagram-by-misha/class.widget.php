<?php
class mishaInstagramWidget extends WP_Widget {

	public $text_domain = 'misha_instagram';


	function __construct() {
		parent::__construct(
			'misha_insta_widget',
			__( 'Instagram', $this->text_domain ),
			array( 'description' => __('Instagram photos by user or by hashtag.', $this->text_domain) )
		);
	}


	public function widget( $args, $instance ) {


		if( ! empty( $args['before_widget'] ) )
			echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) )
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];



		$instance['true_show'] = ( !empty( $instance['true_show'] ) && $instance['true_show'] == 'on' ) ? 'no' : 'yes';
		$instance['true_user'] = !empty( $instance['true_user'] ) ? $instance['true_user'] : '';
		$instance['true_tag'] = !empty( $instance['true_columns'] ) ? $instance['true_tag'] : '';
		$instance['true_columns'] = !empty( $instance['true_columns'] ) ? $instance['true_columns'] : 3;
		$instance['true_count'] = !empty( $instance['true_count'] ) ? $instance['true_count'] : 9;

 		echo do_shortcode('[misha_instagram username="' . $instance['true_user'] . '" tag="' . $instance['true_tag'] . '" heading="' . $instance['true_show'] . '" columns="' . $instance['true_columns'] . '" count="' . $instance['true_count'] . '"]');

 		if( ! empty( $args['after_widget'] ) )
			echo $args['after_widget'];
	}

	/*
	 * бакэнд виджета
	 */
	public function form( $instance ) {




		$title = isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : '';
		$true_user = isset( $instance[ 'true_user' ] ) ? $instance[ 'true_user' ] : '';
		$true_tag = isset( $instance[ 'true_tag' ] ) ? $instance[ 'true_tag' ] : '';
		$true_show = isset( $instance[ 'true_show' ] ) ? $instance[ 'true_show' ] : '';
		$true_columns = isset( $instance[ 'true_columns' ] ) ? $instance[ 'true_columns' ] : 3;
		$true_count = isset( $instance[ 'true_count' ] ) ? $instance[ 'true_count' ] : 9;


		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:') ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'true_user' ); ?>"><?php _e('@Username', $this->text_domain) ?>:</label>
			<input class="widefat true_check_user" id="<?php echo $this->get_field_id( 'true_user' ); ?>" name="<?php echo $this->get_field_name( 'true_user' ); ?>" type="text" value="<?php echo esc_attr( $true_user ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'true_tag' ); ?>"><?php _e('#Tag', $this->text_domain) ?>:</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'true_tag' ); ?>" name="<?php echo $this->get_field_name( 'true_tag' ); ?>" type="text" value="<?php echo esc_attr( $true_tag ); ?>" />
		</p>
		<p>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id( 'true_show' ); ?>" name="<?php echo $this->get_field_name( 'true_show' ); ?>"<?php echo ($true_show == 'on') ? ' checked="checked"' : '' ?>>
			<label for="<?php echo $this->get_field_id( 'true_show' ); ?>"><?php _e('Show photos only, without heading.', $this->text_domain) ?></label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'true_columns' ); ?>"><?php _e('Number of columns', $this->text_domain) ?>:</label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'true_columns' ); ?>" name="<?php echo $this->get_field_name( 'true_columns' ); ?>">
				<option value="1"<?php if($true_columns == '1') echo ' selected="selected"' ?>>1</option>
				<option value="2"<?php if($true_columns == '2') echo ' selected="selected"' ?>>2</option>
				<option value="3"<?php if($true_columns == '3') echo ' selected="selected"' ?>>3</option>
				<option value="4"<?php if($true_columns == '4') echo ' selected="selected"' ?>>4</option>
				<option value="5"<?php if($true_columns == '5') echo ' selected="selected"' ?>>5</option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'true_count' ); ?>"><?php _e('Number of photos', $this->text_domain) ?>:</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'true_count' ); ?>" name="<?php echo $this->get_field_name( 'true_count' ); ?>" type="text" value="<?php echo esc_attr( $true_count ); ?>" />
		</p>
		<?php
	}

	/*
	 * сохранение настроек виджета
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['true_show'] = ( ! empty( $new_instance['true_show'] ) ) ? $new_instance['true_show'] : '';
		$instance['true_user'] = ( ! empty( $new_instance['true_user'] ) ) ? strip_tags( trim( $new_instance['true_user'] ) ) : '';
		$instance['true_tag'] = ( ! empty( $new_instance['true_tag'] ) ) ? strip_tags( trim( $new_instance['true_tag'] ) ) : '';
		$instance['true_columns'] = ( ! empty( $new_instance['true_columns'] ) ) ? $new_instance['true_columns'] : '';
		$instance['true_count'] = ( ! empty( $new_instance['true_count'] ) ) ? $new_instance['true_count'] : '';
		return $instance;
	}
}
