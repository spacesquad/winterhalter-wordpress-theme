<?php
/*
Plugin Name: Instagram Widget+Shortcode by Misha
Plugin URI: https://rudrastyh.com/plugins/instagram-widget-shortcode
Description: The simplest Instagram WordPress plugin in the world.
Version: 7.7.1
Text Domain: misha_instagram
Author: Misha Rudrastyh
Author URI: https://rudrastyh.com

Copyright 2014-2019 Misha Rudrastyh ( https://rudrastyh.com )

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License (Version 2 - GPLv2) as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

class mishaInstagram{

	public $scheme = 'https';
	public $host = 'www.instagram.com';
	public $headers = array(
		'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36',
		'Origin' => 'https://www.instagram.com',
		'Referer' => 'https://www.instagram.com',
		'Connection' => 'close'
	);
	public $text_domain = 'misha_instagram';
	public $version = '7.7.1';
	public $page = 'misha_instagram';
	public $enable_caches = true;
	public $cache_period = 18000; // 5 hrs
	public $upgrade_slug = 'ins50';
	public $plugin_slug = 'instagram-by-misha';
	public $plugin_id = 1038;

	function __construct(){

		add_shortcode( 'instagram', array( $this, 'shortcode' ), 1 );
		add_shortcode( 'misha_instagram', array( $this, 'shortcode' ) );
		add_action( 'wp_head', array( $this, 'css' ) );


		new mishaUpgrade( $this->upgrade_slug, $this->plugin_slug, $this->plugin_id, $this->version, $this->text_domain, array('page'=>'misha_instagram', 'network'=>false) );

	}

	/*
	 * CONNECTION 1
	 * parse user data
	 */
	function serve_user( $username ){

		// first of all - maybe it is in cache
		$cache_key = 'misha_insta_' . $this->version . $username;
		$user_data = get_transient( $cache_key );
		$page_data_matches = array();

		if( $user_data === false || $this->enable_caches === false ) {

			$headers = $this->headers;
			$headers['Host'] = $this->host;

			$response = wp_remote_get( 'https://www.instagram.com/' . $username . '/', array(
				'headers' => $headers
			) );

			//echo '<pre>';print_r( wp_remote_retrieve_header( $response, 'set-cookie' ) );echo '</pre>';
			// if no data
			if ( is_wp_error( $response ) || !preg_match('#window\._sharedData\s*=\s*(.*?)\s*;\s*</script>#', $response['body'], $page_data_matches)) {
				return false;
			}



			$page_data = json_decode($page_data_matches[1], true);
			//echo '<pre>';print_r( $response ); exit;
			// if still no data
			if (!$page_data || empty($page_data['entry_data']['ProfilePage'][0]['graphql']['user']))
				return false;


			$user_data = $page_data['entry_data']['ProfilePage'][0]['graphql']['user'];

			$user_data = array(
				'id' => $user_data['id'],
				'follows' => $user_data['edge_follow']['count'],
				'followed_by' => $user_data['edge_followed_by']['count'],
				'posts' => $user_data['edge_owner_to_timeline_media']['count'],
				'full_name' => $user_data['full_name'],
				'profile_pic_url' => $user_data['profile_pic_url'],
				'profile_pic_url_hd' => $user_data['profile_pic_url_hd'],
				'is_private' => $user_data['is_private'],
				'csrftoken' => $page_data['config']['csrf_token'],
				'cookies' => wp_remote_retrieve_header( $response, 'set-cookie' ),
				'photki' => $user_data['edge_owner_to_timeline_media']['edges']
			);
			//echo '<pre>';print_r( $user_data ); exit;
			set_transient( $cache_key, $user_data, $this->cache_period );

		}

		return $user_data;

	}

	/*
	 * CONNECTION 2
	 * parse response by tag
	 */
	function serve_tag_media( $tag, $limit = 30 ){

		$blacklisted_ids = get_option( 'misha_insta_blacklisted_ids' );
		$blacklisted_ids_key = is_array( $blacklisted_ids ) ? join('_',$blacklisted_ids) : '_';
		$cache_key = 'misha_insta_' . $this->version . $tag . $blacklisted_ids_key . $limit;
		$newnodes = get_transient( $cache_key );

		if( $newnodes === false || $this->enable_caches === false ) {
			//
			// $data_str = json_encode( array(
			// 	'query_id' => '17882293912014529',
			// 	'tag_name' => $tag,
			// 	'first' => $limit
			// ));

			$csrf = uniqid();

			$headers = $this->headers;
			$headers['X-Csrftoken'] = $csrf;
			$headers['X-Requested-With'] = 'XMLHttpRequest';
			$headers['X-Instagram-Ajax'] = '1';
			$headers['Cookie'] = 'csrftoken=' . $csrf;
			$headers['Content-Type'] = 'application/json';
			$headers['Content-Length'] = strlen( $data_str );


			$response = wp_remote_post( $this->scheme . '://' . $this->host . '/explore/tags/' . $tag . '/?__a=1/', array(
				'headers' => $headers,
				//'body' => $data_str
			));


			if ( is_wp_error( $response ) || !preg_match('#window\._sharedData\s*=\s*(.*?)\s*;\s*</script>#', $response['body'], $page_data_matches) )
				return false;


			$page_data = json_decode($page_data_matches[1], true);
			//echo '<pre>';print_r( $page_data ); exit;


			$hashtag_data = $page_data['entry_data']['TagPage'][0]['graphql']['hashtag'];

			// if (!empty($hashtag_data['edge_hashtag_to_media']['edges'])) {
			// 		$count = 12;
			// 		$nodes = array();
			//
			// 		foreach ($hashtag_data['edge_hashtag_to_media']['edges'] as $node) {
			// 				$nodes[] = $node['node'];
			// 		}
			//
			// 		$tag_data['media']['nodes'] = $nodes;
			// 		$raw_data = $tag_data;
			// }
			//print_r( $hashtag_data );exit;
			$newnodes = array();
			// some filter for blacklisted guys


			foreach ($hashtag_data['edge_hashtag_to_media']['edges'] as $item) {

				// skip blacklisted usernames
				if( in_array( $item['node']['owner']['id'], $blacklisted_ids ) ) continue;

				$newnodes[] = $item['node'];
			}
			//echo '<pre>';print_r( $newnodes);exit;
			$newnodes = array_slice($newnodes, 0, $limit);

			set_transient( $cache_key, $newnodes, $this->cache_period );

		}

		return $newnodes;

	}

	/*
	 * CONNECTION 3
	 * parse response by user ID
	 *
	function serve_user_media( $user_id, $limit = 30, $photos ){

		$cache_key = 'misha_insta_' . $this->versioncache . $user_id . '_' . $limit;
		$newnodes = get_transient( $cache_key );

		if( $newnodes === false || $this->enable_caches === false ) {


			$data_str = array(
				'query_id' => '17888483320059182',
				'id' => $user_id,
				'first' => $limit
			);

			//echo $csrf;exit;
			$headers = $this->headers;
			$headers['X-Csrftoken'] = uniqid();

			$headers['X-Requested-With'] = 'XMLHttpRequest';
			$headers['X-Instagram-Ajax'] = '1';
			$headers['Host'] = $this->host;
			//
			//$headers['Content-Type'] = 'application/x-www-form-urlencoded';
			//$headers['Content-Length'] = strlen( $data_str );
			$new_cookies = array();
			foreach( $cookies as $cook ){
				$v = explode(";", $cook);
				$new_cookies[] = $v[0];
			}
			$new_cookies[] = 'ig_or=landscape-primary';
			$new_cookies[] = 'ig_pr=1';
			$new_cookies[] = 'ig_vh=' . rand(500, 1000);
			$new_cookies[] = 'ig_vw=' . rand(1100, 2000);

			$headers['Cookie'] = implode('; ', $new_cookies );


			$proxy_url = null;
			$proxy_credentials = null;

			$curl = curl_init();

			foreach ($headers as $header_key => $header_value) {
	        $headers_raw_list[] = $header_key . ': ' . $header_value;
	    }

			//echo('Что в урле' . $this->scheme . '://' . $this->host . '/graphql/query/?' . http_build_query( $data_str ) );
			// echo('Что в httphederlist' . print_r($headers_raw_list,true) );
			//
			//  exit;
			$curl_options = array(
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_HEADER => true,
					CURLOPT_URL => $this->scheme . '://' . $this->host . '/graphql/query/?' . http_build_query( $data_str ),
					CURLOPT_HTTPHEADER => $headers_raw_list,
					CURLOPT_SSL_VERIFYPEER => false,
					CURLOPT_CONNECTTIMEOUT => 15,
					CURLOPT_TIMEOUT => 60
			);

			curl_setopt_array($curl, $curl_options);

			$response_str = curl_exec($curl);

			@list ($response_headers_str, $response_body_encoded, $alt_body_encoded) = explode("\r\n\r\n", $response_str);

			if ($alt_body_encoded) {
	        $response_headers_str = $response_body_encoded;
	        $response_body_encoded = $alt_body_encoded;
	    }

			if (!$response_body) {
	        $response_body = $response_body_encoded;
	    }


			//echo '<pre>' . print_r( $response_str,true ) . '</pre>';exit;


			$body = json_decode( $response_body, true );
			$nodes = $body['data']['user']['edge_owner_to_timeline_media']['edges'];


			$newnodes = array();

			foreach ($nodes as $item) {
				$newnodes[] = $item['node'];
			}

			$newnodes = array_slice($newnodes, 0, $limit);

			set_transient( $cache_key, $newnodes, $this->cache_period );

		}

		return $newnodes;

	}*/
	function serve_user_media( $username, $limit = 30 ){

		$userdata = $this->serve_user( $username );

		if( !empty( $userdata['is_private'] ) ) {
			return new WP_Error( 'private', __( "This is a private account.", $this->text_domain ) );
		} elseif( isset( $userdata['photki'] ) ) {
			return $this->tipo_serve_user_media( $userdata, $limit  );
		} else {
			return 'Nothing found.';
		}

	}


	function tipo_serve_user_media( $userdata, $limit ){

		// никакого кэша нам нафик не нужно, потому что всё кэшируется на serve_user()
		$nodes = $userdata['photki'];
		$newnodes = array();

		foreach ($nodes as $item) {
			$newnodes[] = $item['node'];
		}

		// теперь мы ограничены 12 фотами
		$limit = ( $limit > 12) ? 12 : $limit;

		$newnodes = array_slice($newnodes, 0, $limit);

		return $newnodes;

	}

	/*
	 * Converts 5500 to 5.5K
	 * Converts 7109904 to 7.1M
	 */
	function format_count( $n ) {

		$unit = '';

        if ($n < 1000) {
            return $n;
        } else if ($n > 1000000) {
            $factor = $n / 1000000;
            $unit = __("M", $this->text_domain );
        } else if ($n > 1000) {
            $factor = $n / 1000;
            $unit = __("K", $this->text_domain );
        }
        if (intval($factor, 10) !== $factor) {
            $factor = round( $factor, 1 );
        }
        $formatted = $factor . $unit;
        return $formatted;
    }

    /*
     * Creates HTML tags for Instagram response
     * returns the result HTML
     */
    function format_to_html( $nodes ){

			if( empty( $nodes ) )
    		return '<div class="misha_insta_err">Nothing found.</div>';

    	if( is_wp_error( $nodes ) )
    		return '<div class="misha_insta_err">' . $nodes->get_error_message() . '</div>';

    	$html = '';

    	$links = get_theme_mod('misha_insta_link_to');
			if( empty( $links ) )  $links = 'instagram';

    	foreach( $nodes as $node ) :

    		$class = $node['is_video'] ? ' misha-insta-video' : '';

    		$html .= '<div id="instaid-' . $node['id'] . '" class="misha-insta-photo' . $class . '">';
    		$img = '<img src="' . $node['thumbnail_src'] . '" alt="">'; //esc_attr( $node['edge_media_to_caption']['edges'][0]['node']['text'] )

				if( $links === 'none' && !$node['is_video'] ) {
    			$html .= $img;
    		} else {
    			$url = ( $links === 'instagram' || $node['is_video'] ) ? 'https://instagram.com/p/' . $node['shortcode'] : $node['display_url'];
					$html .= '<a href="' . $url . '" target="_blank">' . $img . '</a>';
				}
				$html .= '</div>';
    	endforeach;
    	return $html;
    }

    /*
     * Widget + Shortcode CSS
     * It will be added in wp_head() to reduce HTTP request numbers
     */
	function css(){

		// do nothing if CSS is disabled in settings
		if( get_option('misha_insta_no_plugin_css' ) == 'on' ) return;

		$buttoncolor = ($x = get_theme_mod( 'misha_insta_follow1' ) ) ? $x : '#09c';
		$buttonhover = ($x = get_theme_mod( 'misha_insta_follow2' ) ) ? $x : '#068';
		?>
		<style>
		/*resets*/
		.misha-insta a, .misha-insta img{
			outline:0 !important;
			border:0 !important;
			text-decoration:none !important;
			-webkit-box-shadow:none !important;
			box-shadow:none !important
		}
		.misha-insta-header {
			margin-bottom: 0.8em;
		}
		.misha-insta-header img{
			display:block;
			-moz-border-radius:50%;
			-webkit-border-radius:50%;
			border-radius:50%;
		}
		.misha-insta-header a {
    	display: inline-block;
    	color: #000;
		}
		.misha-insta-header a:last-child{
			padding-left: 12px;
			line-height: 40px;
			vertical-align: top;
		}
		.misha-insta-counts div span{
			display:block
		}
		.misha-insta-counts{
			overflow:hidden;
		}
		.misha-insta .misha-insta-followlink,.misha-insta .misha-insta-followlink:focus{
			border:1px solid <?php echo $buttoncolor ?>!important;
			font-size:0.8em;
			padding:0.5em 1em;
			border-radius:3px;
			display:block;
			color:<?php echo $buttoncolor ?>;
			margin-left:auto;
			float:right;
		}
		.misha-insta .misha-insta-followlink:hover,.misha-insta .misha-insta-followlink:active{
			border-color:<?php echo $buttonhover ?>!important;
			color:<?php echo $buttonhover ?>;
		}
		.misha-insta-counts>div{
			padding-right:20px;
			float:left;
		}
		.misha-insta-nums{
			font-weight:bold;
			line-height:1;
			text-align:center
		}
		.misha-insta-labels{
			font-size:0.8em;
			line-height:1;
			margin-top:4px
		}
		.misha-insta-photos{
			box-sizing:border-box;
			margin-top:1em;
			margin-right:-1px;
			margin-left:-1px;
			overflow:hidden;
		}
		.misha-insta-photo{
			display:block;
			float:left;
			max-width:33.333333%;
			padding-right:1px;
			padding-left:1px;
			padding-bottom:2px
		}
		.misha-insta-columns2 .misha-insta-photo{max-width:50%}
		.misha-insta-columns4 .misha-insta-photo{max-width:25%}
		.misha-insta-columns5 .misha-insta-photo{max-width:20%}
		.misha-insta-photo img{display:block;max-width:100%;height:auto}
		@media screen and (max-width:638px){
			.misha-insta .misha-insta-photo{max-width:50%}
		}
		@media screen and (max-width:408px){
			.misha-insta .misha-insta-photo{
				float:none;
				max-width:100%
			}
		}
		.misha-insta-columns1 .misha-insta-photo{
			max-width:100%;
			float:none
		}
		</style>
		<?php
	}


	/*
	 * Shortcode itself
	 */
	function shortcode( $atts ){

		// prepare arguments
		$params = shortcode_atts( array(
			'username' => '',
			'tag' => '',
			'heading' => 'yes',
			'count' => 9,
			'columns' => 3
		), $atts );


		// if no user and no tag, return nothing
		if( $params['username'] === '' && $params['tag'] === '' )
			return;

		if( !is_int( intval($params['count'] ) ) ) $params['count'] = 9;
		if( $params['count'] < 1 ) $params['count'] = 1;
		if( $params['count'] > 200 ) $params['count'] = 200;

		$params['username'] = str_replace( '@', '', $params['username'] );
		$params['tag'] = str_replace( '#', '', $params['tag'] );

		$html = '<div class="misha-insta misha-insta-columns' . $params['columns'] . '">';

		if( $params['username'] !== '' ) :

			$userdata = $this->serve_user( $params['username'] );

			// if turn on heading
			if( $userdata && $params['heading'] === 'yes' )
				$html .= '<div class="misha-insta-header">
					<a href="https://instagram.com/' . $params['username'] . '" target="_blank">
						<img width="40" height="40" alt="' . $userdata['full_name'] . '" src="' . $userdata['profile_pic_url'] . '" />
					</a>
					<a href="https://instagram.com/' . $params['username'] . '" target="_blank"><span>' . $params['username'] . '</span></a>
				</div>
				<div class="misha-insta-counts">
					<div>
						<span class="misha-insta-nums">' . $this->format_count( $userdata['posts'] ) . '</span>
						<span class="misha-insta-labels">' . __('posts', $this->text_domain ) . '</span>
					</div>
					<div>
						<span class="misha-insta-nums">' . $this->format_count( $userdata['followed_by'] ) . '</span>
						<span class="misha-insta-labels">' . __('followers', $this->text_domain ) . '</span>
					</div>
					<div>
						<span class="misha-insta-nums">' . $userdata['follows'] . '</span>
						<span class="misha-insta-labels">' . __('following', $this->text_domain ) . '</span>
					</div>
					<a href="https://instagram.com/' . $params['username'] . '" title="' . sprintf( __('Follow %s in Instagram'), $params['username'] ) . '" onclick="window.open(this.href, this.title, \'toolbar=0, status=0, width=430, height=500\'); return false" target="_parent" class="misha-insta-followlink">
						' . __('Follow', $this->text_domain ) . '
					</a>
				</div>';

			if( !empty( $userdata['is_private'] ) ) {
				$response = new WP_Error( 'private', __( "This is a private account.", $this->text_domain ) );
			} else {
				$response = $this->tipo_serve_user_media( $userdata, $params['count']  );
			}

		else:

			$response = $this->serve_tag_media( $params['tag'], $params['count'] );

		endif;

		$html .= '<div class="misha-insta-photos">' . $this->format_to_html( $response ) . '</div>';
		$html .= '</div>';

		return $html;

	}

}


require_once( dirname( __FILE__ ) . '/class.widget.php' );
require_once( dirname( __FILE__ ) . '/class.upgrade.php' );
require_once( dirname( __FILE__ ) . '/class.options.php' );
