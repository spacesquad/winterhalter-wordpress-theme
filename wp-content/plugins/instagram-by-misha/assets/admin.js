jQuery(function($){

		$('.inscolorfield').wpColorPicker();

		/* if plugin CSS is disabled, hide the unneeded options */
		$('#no_plugin_css').change(function(){
			if( $(this).is(':checked') ) {
				$('.instagram-plugin-css').hide();
			} else {
				$('.instagram-plugin-css').show();
			}
		});

});
