(function() {
	tinymce.PluginManager.add('misha_insta_button_name', function( editor, url ) {
		editor.addButton( 'misha_insta_button_name', {
			icon: 'trueinsta',
			title: misha_insta_obj.param1,
			
							onclick: function() {
								editor.windowManager.open( {
									title: misha_insta_obj.param2,
									body: [
										/* yuzer */
										{
											id: 'tnmcebuttonbyval',
											type: 'textbox',
											name: 'textbox1Name',
											label: misha_insta_obj.text1,
											value: ''
										},
										/* heshteg */
										{
											type: 'textbox',
											name: 'textbox2Name',
											label: misha_insta_obj.text2,
											value: ''
										},
										/* zagolovok */
										{
											type: 'listbox',
											name: 'listbox1Name',
											label: misha_insta_obj.listbox1,
											'values': [
												{text: misha_insta_obj.listbox1_1, value: 'yes'},
												{text: misha_insta_obj.listbox1_2, value: 'no'}
											]
										},
								
										/* columns */
										{
											id: 'tnmcebuttoncount',
											type: 'listbox',
											name: 'listbox4Name',
											label: misha_insta_obj.listbox4,
											value: '3',
											'values': [
												{text: '1', value: '1'},
												{text: '2', value: '2'},
												{text: '3', value: '3'},
												{text: '4', value: '4'},
												{text: '5', value: '5'}
											]
										},
										/* number of photos */
										{
											type: 'textbox',
											name: 'textbox3Name',
											label: misha_insta_obj.text3,
											value: '9'
										}
										
									],
									onsubmit: function( e ) { // это будет происходить после заполнения полей и нажатии кнопки отправки
										editor.insertContent( '[instagram username="' + e.data.textbox1Name + '" tag="' + e.data.textbox2Name + '" heading="' + e.data.listbox1Name + '" columns="' + e.data.listbox4Name + '" count="' + e.data.textbox3Name + '"]');
									}
								});
							}
								
				
		});
	});
})();