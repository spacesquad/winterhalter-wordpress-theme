jQuery(function($){

	
	wp.customize( 'misha_insta_follow1', function( value ) {
		value.bind( function( to ) {
			$('.misha-insta-followlink').attr('style', 'color:'+to+';border-color:'+to+' !important');
		} );
	});

	wp.customize( 'misha_insta_follow2', function( value ) {
		value.bind( function( to ) {
		
			$('.misha-insta-followlink').attr('data-hover-style', 'color:'+to+';border-color:'+to+' !important');
		} );
	});


	$('.misha-insta-followlink').mouseover(function() {
		$(this).attr('data-style', $(this).attr('style'));
		$(this).attr('style', $(this).attr('data-hover-style'));
	}).mouseout(function() {
		$(this).attr('style', $(this).attr('data-style'));
	});

});