<?php

class mishaInstagramOptions extends mishaInstagram{

	function __construct(){

		parent::__construct();

		add_action( 'admin_menu', array( $this, 'add' ) );
		add_action( 'admin_menu', array( $this, 'save' ) ); // yes, yes, I know
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
		add_action( 'admin_head', array( $this, 'mce_button' ) );
		add_action( 'customize_register', array( $this, 'customizer'), 9999 );
		add_action( 'customize_preview_init', array( $this, 'customizer_js' ) );
		add_action( 'widgets_init', array( $this, 'widget' ) );

		add_action( 'plugins_loaded', array( $this, 'lang' ) );
		// тут просто ссылка на настройки плагина
		add_filter( 'plugin_action_links_instagram-by-misha/instagram-by-misha.php', array( $this, 'link_to_settings' ) );
	}



	/*
	 * Функция, которая будет генеририть нотисы, а то задолбался))
	 */
	function notice( $text, $type = 'warning', $echo = true ) {
		$notice = '<div class="notice notice-' . $type . '"><p>' . $text . ' </p></div>';
		echo $notice;
	}
	/*
	 * Просто ссылка на настройки со страницы плагинов
	 */
	function link_to_settings( $links ){
		return array_merge( array(
 			'<a href="' . admin_url( 'options-general.php?page=' . $this->page ) . '">' . __('Settings') . '</a>',
 		), $links );
	}
	/**
	 * Just add new options page here
	 * We use default WordPress add_options_page()
	 */
	function add(){
		add_options_page(
			__('Instagram Settings', $this->text_domain ),
			__('Instagram', $this->text_domain ),
			'manage_options',
			$this->page,
			array( $this, 'html' )
		);
	}

	/**
	 * Add options page HTML, tabs
	 */
	function html(){

		?><div class="wrap"><h1><?php _e('Instagram Settings', $this->text_domain ) ?></h1>
		<?php
		/**
		 * Всё, что касается notices. Я понимаю, что могу повесить на хук admin_notices, но тут я думаю нет необходимости
		 */
		if( isset( $_GET['saved'] ) ) {
			switch( $_GET['saved'] ) :
				default : {
					$this->notice( '<strong>' . __('Settings saved.') . '</strong>', 'success');
					break;
				}
			endswitch;
		}
		?>
		<form method="post" action=""><?php wp_nonce_field( 'update' . get_current_user_id(), '_instagram_settings_nonce' ); ?>
		<table class="form-table">
			<tbody>
				<?php
				/**
				 * Лицензионный ключ плагина
				 * Выше мы её уже получили в $license, поэтому тут просто выводим
				 * Различные уведомления, связанные с лицензией, работают в class.upgrade, поэтому тут нам нужно просто вывести
				 */
				$license = is_multisite() ? get_site_option('_misha_' . $this->plugin_slug . '_license_key') : get_option('_misha_' . $this->plugin_slug . '_license_key');
				?>
				<tr>
					<th scope="row">
						<label for="mishinsta_license_key"><?php _e( 'License key', $this->text_domain ) ?></label>
					</th>
					<td>
						<input class="regular-text" spellcheck="false" type="text" id="mishinsta_license_key" name="mishinsta_license_key" autocomplete="off" value="<?php echo $license ? substr_replace( $license, '*****', - ( strlen($license) - 3 ) ) : ''; ?>">
						<p class="description"><?php _e('License key is required to receive the latest plugin updates.', $this->text_domain); ?></p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">
						<label for="blacklisted_usernames"><?php _e('Blacklisted Usernames', $this->text_domain ); ?></label>
					</th>
					<td>
						<textarea id="blacklisted_usernames" name="blacklisted_usernames" cols="80" rows="5"><?php echo ( $blacklisted = get_option( 'misha_insta_blacklisted_users' ) ) ? implode("\n", $blacklisted ) : ''; ?></textarea>
						<p class="description"><?php _e('The content of these users won\'t be displayed in the feed while displaying media by a tag.<br />Each username must be on the new line.', $this->text_domain); ?></p>
					</td>
				</tr>
			</tbody>
		</table>
		<?php
		/*
		 * Appearance Settings (separate table)
		 */
		$links = ( $links = get_theme_mod('misha_insta_link_to') ) ? $links : 'instagram';

		?>
		<h2 class="title"><?php _e('Appearance', $this->text_domain ) ?></h2>
		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row">Custom Style</th>
					<td>
						<label for="no_plugin_css"><input type="checkbox" id="no_plugin_css" name="no_plugin_css" <?php checked( 'on', get_option('misha_insta_no_plugin_css' ) ) ?>> Remove plugin CSS from <code>&lt;head&gt;</code></label>
					</td>
				</tr>
				<tr class="instagram-plugin-css"<?php if( get_option('misha_insta_no_plugin_css' ) == 'on' ) echo ' style="display:none"'; ?>>
					<th scope="row">
						<label for="misha_insta_follow1"><?php _e('Follow Button Color', $this->text_domain ) ?></label>
					</th>
					<td>
						<input class="regular-text inscolorfield" type="text" id="misha_insta_follow1" name="misha_insta_follow1" value="<?php echo ($x = get_theme_mod( 'misha_insta_follow1' ) ) ? $x : '#09c' ?>" />
					</td>
				</tr>
				<tr class="instagram-plugin-css"<?php if( get_option('misha_insta_no_plugin_css' ) == 'on' ) echo ' style="display:none"'; ?>>
					<th scope="row">
						<label for="misha_insta_follow2"><?php _e('Follow Button Color on hover', $this->text_domain ) ?></label>
					</th>
					<td>
						<input class="regular-text inscolorfield" type="text" id="misha_insta_follow2" name="misha_insta_follow2" value="<?php echo ($x = get_theme_mod( 'misha_insta_follow2' ) ) ? $x : '#068' ?>" />
					</td>
				</tr>
				<tr>
					<th scope="row">
						<label for="misha_insta_link_to"><?php _e('Link To', $this->text_domain ) ?></label>
					</th>
					<td>
						<select id="misha_insta_link_to" name="misha_insta_link_to">
							<option value="none" <?php selected('none', $links) ?>><?php _e('None', $this->text_domain ) ?></option>
							<option value="instagram" <?php selected('instagram', $links) ?>><?php _e('Instagram', $this->text_domain ) ?></option>
							<option value="media" <?php selected('media', $links) ?>><?php _e('Media File', $this->text_domain ) ?></option>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
		<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
		</p>
		</form>
		</div>
		<?php

	}



	function save(){

		// do nothing if we're in wrong place
		if( empty( $_GET['page'] ) || $_GET['page'] !== $this->page )
			return;

		if ( ! isset( $_POST['_instagram_settings_nonce'] ) || ! wp_verify_nonce( $_POST['_instagram_settings_nonce'], 'update' . get_current_user_id() ) )
			return;


		/**
		 * Начнём с лицензионного ключа
		 */
		if( !preg_match("/\*\*$/", $_POST['mishinsta_license_key'] ) ) {

			// обновить, если не заканчивается на звездочки
			if( is_multisite() ) {
				update_site_option( '_misha_' . $this->plugin_slug . '_license_key', $_POST['mishinsta_license_key'] );
				delete_site_transient( 'misha_upgrade_' . $this->upgrade_slug );
			} else {
				update_option( '_misha_' . $this->plugin_slug . '_license_key', $_POST['mishinsta_license_key'] );
				delete_transient( 'misha_upgrade_' . $this->upgrade_slug );
			}

		}

		// blacklisted
		$blacklisted_new = array_map('trim', explode("\n", $_POST['blacklisted_usernames'] ) );
		$blacklisted_ids_new = array();

		if( $blacklisted_new ) {
			foreach( $blacklisted_new as $username_to_get ) {
				$user_data = $this->serve_user( $username_to_get );
				if( isset( $user_data['id'] ) ) $blacklisted_ids_new[] = $user_data['id'];
			}
		}
		update_option( 'misha_insta_blacklisted_users', $blacklisted_new );
		update_option( 'misha_insta_blacklisted_ids', $blacklisted_ids_new );


		// disable css
		update_option( 'misha_insta_no_plugin_css', $_POST['no_plugin_css'] );

		// appearance settings
		set_theme_mod( 'misha_insta_follow1', $_POST['misha_insta_follow1'] );
		set_theme_mod( 'misha_insta_follow2', $_POST['misha_insta_follow2'] );
		set_theme_mod( 'misha_insta_link_to', $_POST['misha_insta_link_to'] );




		wp_safe_redirect( add_query_arg( array(
			'page' => $this->page,
			'saved' => 1
		), 'options-general.php' ));

		exit;
	}



	/*
	 * Admin Script + Style registration
	 */
	function admin_scripts(){
		wp_enqueue_style( 'wp-color-picker' );

		wp_enqueue_style('misha_insta_css', plugin_dir_url( __FILE__ ) . 'assets/admin.css', $this->version );
		wp_enqueue_script('misha_insta_js', plugin_dir_url( __FILE__ ) . 'assets/admin.js', array('jquery','wp-color-picker'), $this->version );

		// localize
		wp_localize_script( 'misha_insta_js', 'misha_insta_obj', array(
			'param1' => __( 'Insert Instagram Shortcode', $this->text_domain ),
			'param2' => __( 'Shortcode Parameters', $this->text_domain ),
			'text1' => __( '@User', $this->text_domain ),
			'text2' => __( '#Tag', $this->text_domain ),
			'listbox1' => __( 'Display heading', $this->text_domain ),
			'listbox1_1' => __( 'Show', $this->text_domain ),
			'listbox1_2' => __( 'Hide', $this->text_domain ),
			'listbox4' => __('Number of columns', $this->text_domain),
			'text3' => __( 'Number of photos', $this->text_domain )

		) );
	}

	/*
	 * All about MCE button registration
	 */
	function mce_button() {
		if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) )
			return;

		if ( 'true' == get_user_option( 'rich_editing' ) ) {
			add_filter( 'mce_external_plugins', array( $this, 'mce_script' ) );
			add_filter( 'mce_buttons', array( $this, 'mce_register' ) );
		}
	}
	function mce_script( $plugin_array ) {
		$plugin_array['misha_insta_button_name'] = plugin_dir_url( __FILE__ ) .'assets/editor_button.js';
		return $plugin_array;
	}
	function mce_register( $buttons ) {
		array_push( $buttons, 'misha_insta_button_name' );
		return $buttons;
	}

	/*
	 * Customizer below
	 */
	function customizer( $wp_customize ){

		if( get_option('misha_insta_no_plugin_css' ) == 'on' ) return;

		$wp_customize->add_section(
		'misha_insta', array(
				'title'     => __( 'Instagram', $this->text_domain ),
				'priority'  => 200,
				'description' => __( 'Configure Instagram Widget Appearance here', $this->text_domain )
			)
		);

		$wp_customize->add_setting(
			'misha_insta_follow1', // id
			array(
				'default'     => '#09c',
				'capability'     => 'edit_theme_options',
				'transport'   => 'postMessage'
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'misha_insta_follow1',
				array(
			    	'label'      => __( 'Follow Button Color', $this->text_domain ),
			    	'section'    => 'misha_insta',
			    	'settings'   => 'misha_insta_follow1'
				)
			)
		);
		$wp_customize->add_setting(
			'misha_insta_follow2', // id
			array(
				'default'     => '#068',
				'capability'     => 'edit_theme_options',
				'transport'   => 'postMessage'
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'misha_insta_follow2',
				array(
			    	'label'      => __( 'Follow Button Color on hover', $this->text_domain ),
			    	'section'    => 'misha_insta',
			    	'settings'   => 'misha_insta_follow2'
				)
			)
		);
		$wp_customize->add_setting('misha_insta_link_to', array(
        'default'        => 'instagram',
        'capability'     => 'edit_theme_options',
        'transport'   => 'refresh'

    ));
    $wp_customize->add_control( 'misha_insta_link_to', array(
        'settings' => 'misha_insta_link_to',
        'label'   => __( 'Link To', $this->text_domain ),
        'section' => 'misha_insta',
        'type'    => 'select',
        'choices' => array(
        	'none' => __('None', $this->text_domain ),
        	'instagram' => __( 'Instagram', $this->text_domain ),
        	'media' => __( 'Media File', $this->text_domain )
        ),
    ));

	}

	function customizer_js(){
		wp_enqueue_script('misha_insta_customizer', plugin_dir_url( __FILE__ ) . 'assets/customizer.js', array('jquery','customize-preview'), $this->version, true );
	}

	function widget(){

		register_widget( 'mishaInstagramWidget' );

	}




    function lang(){

    	load_plugin_textdomain( $this->text_domain, false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );

    }



}

new mishaInstagramOptions();
