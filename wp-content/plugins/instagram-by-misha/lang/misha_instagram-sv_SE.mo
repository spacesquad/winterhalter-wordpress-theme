��    #      4  /   L        �   	     �     �  	   �  *   �     �     �     �          #     @     E  	   `  #   j  '   �     �     �     �  
   �     �     �     �     �            "     6   B     y     �  	   �  	   �     �  8   �     �  -    �   2	     �	     �	     �	  *   �	     
     $
     *
     A
     W
     u
     {
  	   �
  %   �
  0   �
     �
  
   �
                         %     4     A     V     [  8   u     �     �     �     �     �  8   �     1                                                                            	         !      "                         #               
                           Please&nbsp;<a href="%1$s">renew&nbsp;your&nbsp;license</a>&nbsp;to&nbsp;update. And make sure that your billing email is set in Settings &gt; General #Tag @User @Username Configure Instagram Widget Appearance here Display heading Follow Follow %s in Instagram Follow Button Color Follow Button Color on hover Hide Insert Instagram Shortcode Instagram Instagram Widget+Shortcode by Misha Instagram photos by user or by hashtag. K Link To M Media File Misha Rudrastyh None Number of columns Number of photos Shortcode Parameters Show Show photos only, without heading. The simplest Instagram WordPlress plugin in the world. This is a private account. Title: followers following https://rudrastyh.com https://rudrastyh.com/plugins/instagram-widget-shortcode posts Project-Id-Version: Instagram Widget+Shortcode by Misha
POT-Creation-Date: 2018-01-27 11:21+0300
PO-Revision-Date: 2018-01-27 11:24+0300
Last-Translator: Fredric Österlund <info@fredo.nu>
Language-Team: Misha Rudrastyh
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.3
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: instagram-by-misha.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: .
X-Poedit-SearchPathExcluded-0: *.js
  Vänligen <a href="%1$s">förläng din licens</a> för att uppdatera. Och se till att din faktureringsadress är inställd i Inställningar &gt; Allmänt #Tag @Användare @Användarnamn Konfigurera utseende för Instagram widget Visa rubrik Följ Följ %s på Instagram Färg på följaknapp Hovringsfärg på följaknapp Dölj Infoga Instagram shortcode Instagram Instagram Widget + shortcode av Misha Instagram bilder av användare eller av hashtag. K Länk till M Mediafil Misha Rudrastyh Ingen Antal kolumner Antal bilder Shortcode parametrar Visa Visa bilder, utan rubrik. Världens enklaste Wordpressplugin för Instagrambilder. Detta är ett privat konto. Titel: följare följer https://rudrastyh.com https://rudrastyh.com/plugins/Instagram-widget-Shortcode inlägg 