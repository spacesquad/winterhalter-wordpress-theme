<?php
	
	
	/*
		
		
		Plugin Name: GDY Cookie Note
		Tags: gdy, cookie, note
		Plugin URI: https://wordpress.org/plugins/gdy-cookie-note/
		Author: Johannes Grandy
		Author URI: http://johannesgrandy.com/
		Text Domain: gdy-cookie-note
		Domain Path: /languages
		Description: A very lightweight cookie plugin without any dependencies or extra files loaded.
		License: GPLv2 or later
		License URI: http://www.gnu.org/licenses/gpl-2.0.html
		Copyright: Johannes Grandy
		Version: 0.2.0
		Build: 1

	
	*/
	



	/*********************************** TRANSLATION *********************************/
	
	load_plugin_textdomain( 'gdy-cookie-note', false, plugin_basename( __DIR__ ) . '/languages/' );
	



	/*********************************** SELECTORS *********************************/

	$gdycn_selectors = apply_filters( 'gdycn_selectors', array(

		'body' => 'gdycn_visible',

		'note_container' => 'gdycn_container',
		'note_inner' => 'gdycn_inner',
		'note_close' => 'gdycn_close',
		'note_text' => 'gdycn_text',


	) );





	/*************************** NOTE ***************************/

	add_action( 'wp_footer', 'gdycn_note' );

	function gdycn_note() {
		

		global $gdycn_selectors;


		$text = apply_filters( 'gdycn_text', __( 'Cookies: This website uses cookies to function properly and to provide you with an optimal user experience. If you agree, continue your visit on our website.', 'gdy-cookie-note' ) );


		echo '<div id="' . $gdycn_selectors[ 'note_container' ] . '">';

			echo '<div id="' . $gdycn_selectors[ 'note_inner' ] . '">';

				echo '<div id="' . $gdycn_selectors[ 'note_close' ] . '"><span>&times;</span></div>';

				echo '<div id="' . $gdycn_selectors[ 'note_text' ] . '">' . $text . '</div>';

			echo '</div>';

		echo '</div>';


	}



	/*************************** STYLE ***************************/

	add_action( 'wp_footer', 'gdycn_style' );

	function gdycn_style() {

		global $gdycn_selectors;

	?>
			
		<style>
			
			#<?php echo $gdycn_selectors[ 'note_container' ]; ?> {
				display: none;
			    position: fixed;
			    bottom: 0px;
			    left: 0px;
			    right: 0px;
			    color: #fff;
			    padding: 15px 15px 15px 80px;
			    z-index: 10000;
				background: #ffb900;
			}

			.<?php echo $gdycn_selectors[ 'body' ]; ?> #<?php echo $gdycn_selectors[ 'note_container' ]; ?> {
				display: block;
			}

			#<?php echo $gdycn_selectors[ 'note_text' ]; ?> a {
			    color: #fff;
			    border-bottom: solid 1px rgba(255,255,255,0.3);
			}

			#<?php echo $gdycn_selectors[ 'note_close' ]; ?> {
		        position: absolute;
			    top: 0px;
			    left: 0px;
			    width: 60px;
			    height: 100%;
			    cursor: pointer;
			    border-right: solid 1px rgba(255,255,255,0.2);
			    overflow: hidden;
				background: none;
			}

			#<?php echo $gdycn_selectors[ 'note_close' ]; ?> span {
		        position: absolute;
			    top: 50%;
			    left: 50%;
			    width: 30px;
			    height: 30px;
			    line-height: 30px;
			    text-align: center;
			    margin-top: -15px;
			    margin-left: -15px;
			    font-size: 1.5em;
			}

		</style>

	<?php

	}


	/*************************** SCRIPT ***************************/

	add_action( 'wp_footer', 'gdycn_script' );

	function gdycn_script() {
		
		global $gdycn_selectors;
	?>
		
		<script>

			if( !localStorage.hideGDYCN ) {

				document.body.classList.add( '<?php echo $gdycn_selectors[ 'body' ]; ?>' );

			}

			document.getElementById( '<?php echo $gdycn_selectors[ 'note_close' ]; ?>' ).onclick = function() {

				localStorage.hideGDYCN = true;

				document.body.classList.remove( '<?php echo $gdycn_selectors[ 'body' ]; ?>' );

			}

		</script>

	<?php

	}



	



	
	
	
?>