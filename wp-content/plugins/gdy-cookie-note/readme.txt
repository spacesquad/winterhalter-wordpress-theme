=== GDY Cookie Note ===

Contributors: Grandy, fouadvollmer
Plugin Name: GDY Cookie Note
Tags: gdy, cookie, note
Plugin URI: https://wordpress.org/plugins/gdy-cookie-note/
Author: Johannes Grandy
Author URI: http://johannesgrandy.com/
Text Domain: gdy-cookie-note
Domain Path: /languages
Description: The most lightweight yet customizable cookie plugin with hook based settings.
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Copyright: Johannes Grandy
Requires at least: 3.6
Tested up to: 4.6
Stable tag: 0.7.9

A very lightweight cookie plugin without any dependencies or extra files loaded.

== Description ==

Another plugin that bloats my backend with option pages and settings? No! This plugin is a drop in cookie note solution. Just install it and it works. 

It is just a 4 KB big file with only 3 hooks (the note, script and style). It has almost no programming overhead because customiztation runs via hooks, filters and css.

If you want to make adjustments that go beyond some CSS styles you should be familiar with WordPress filters and hooks. If you are not, there are probably better plugins for you.

For customization examples view the plugins FAQ.

== Installation ==

Here's how to install the plugin:

1. Upload 'gdy-cookie-note' to the '/wp-content/plugins/' directory
1. Activate the plugin through the 'Plugins' menu in WordPress


== Frequently Asked Questions ==

= Is it possible to change the position of the note =

Yes. You can overwrite the default CSS styles or completely deactivate them.

`
remove_action( 'wp_footer', 'gdycn_style' );
`


= Is it possible to change the color of the note =

Yes. You can overwrite the default CSS styles or completely deactivate them.

`
remove_action( 'wp_footer', 'gdycn_style' );
`


= Is there a way to style elements depending on the visibility of the note =

Yes. The `gdycn_visible` class is added to the body if the note is visible.


= Is it possible to change the ids or classes =

Yes. Use the `gdycn_selectors` filter:

`
add_filter( 'gdycn_selectors', 'custom_cookie_class' );

function custom_cookie_class( $selectors ) {
    
    $selectors[ 'body' ] = 'my_custom_body_class';

    return $selectors;

}
`


= Is it possible to customize the text =

Yes. Use the `gdycn_text` filter:

`
add_filter( 'gdycn_text', 'custom_cookie_message' );

function custom_cookie_message( $text) {

    return 'My own cookie message';

}
`


= Is it possible to completely change the window =

Yes. Just remove the GDYCN hooks and add your own window anywhere. You can use the existing javascript with your custom window. The visibility is then controled by the body class added by the script.

`
remove_action( 'wp_footer', 'gdycn_note' );
remove_action( 'wp_footer', 'gdycn_style' );

// Add your own HTML and CSS
`

= How can i close my custom window =

If the GDYCN JS is still enabled a click on `#gdycn_close` should remove the body class (which control the visiblity of the window). Make sure you use the correct id if you have changed them via `gdycn_selectors`.


= Is there a JS callback when the window is rendered =

No. Just look for the body class `cookie_note`.




== Changelog ==

= 0.2.0 =
* Changed to js local storage instead of cookies. No dependencies (jQuery.js, cookie.js ...) or any extra files (js, css) needed anymore. The whole plugin is even smaller now.

= 0.1.0 =
* Initial release.


